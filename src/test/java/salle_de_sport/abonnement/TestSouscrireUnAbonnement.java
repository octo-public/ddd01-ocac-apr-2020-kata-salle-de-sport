package salle_de_sport.abonnement;

import org.junit.jupiter.api.Test;
import salle_de_sport.abonnement.domain.AbonnementAnnuelSouscrit;

import static org.assertj.core.api.Assertions.assertThat;

class TestSouscrireUnAbonnement {
    @Test
    void la_reduction_annuelle_est_appliquee_quand_on_souscrit_a_un_abonnement_annuel() {
        // Given
        AbonnementRepository abonnementRepository = new InMemoryAbonnementRepository();
        SouscrireUnAbonnementAnnuel souscrireUnAbonnementAnnuel = new SouscrireUnAbonnementAnnuel(
                "NomDeLaformule",
                new PrixDeBase(100),
                "clientId"
        );

        // When
        DomainEvent event = new SouscrireUnAbonnementAnnuelUseCase(abonnementRepository).execute(souscrireUnAbonnementAnnuel);

        // Then
        assertThat(new AbonnementAnnuelSouscrit("abonnementId", "NomDeLaformule", new PrixDAbonnement(70))).isEqualTo(event);
    }

    @Test
    void l_abonnement_annuel_a_ete_sauvegarde_lors_la_souscription() {
        // Given
        AbonnementRepository abonnementRepository = new InMemoryAbonnementRepository();
        SouscrireUnAbonnementAnnuel souscrireUnAbonnementAnnuel = new SouscrireUnAbonnementAnnuel(
                "NomDeLaformule",
                new PrixDeBase(100),
                "clientId"
        );

        // When
        new SouscrireUnAbonnementAnnuelUseCase(abonnementRepository).execute(souscrireUnAbonnementAnnuel);

        // Then
        Abonnement expectedAbonnement = abonnementRepository.getById("abonnementId");
        assertThat(Abonnement.souscrireAbonnementAnnuel("abonnementId", "clientId", souscrireUnAbonnementAnnuel.prixDeBase, souscrireUnAbonnementAnnuel.nomDeLaFormule)).isEqualTo(expectedAbonnement);
    }
}
