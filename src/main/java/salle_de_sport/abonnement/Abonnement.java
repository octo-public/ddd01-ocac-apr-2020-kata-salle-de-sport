package salle_de_sport.abonnement;

import salle_de_sport.abonnement.domain.AbonnementAnnuelSouscrit;

import java.util.Objects;

public class Abonnement {
    private static final Reduction REDUCTION_ANNUELLE = new Reduction(0.3);

    public final String abonnementId;
    public final PrixDAbonnement prixDAbonnement;
    private final String clientId;
    private final String nomDeLaFormule;

    private DomainEvent event;

    public static Abonnement souscrireAbonnementAnnuel(String abonnementId, String clientId, PrixDeBase prixDeBase, String nomDeLaFormule) {
        PrixDAbonnement prixDAbonnement = prixDeBase.appliquer(REDUCTION_ANNUELLE);
        Abonnement abonnement = new Abonnement(abonnementId, clientId, prixDAbonnement, nomDeLaFormule);
        abonnement.event = new AbonnementAnnuelSouscrit(abonnementId, nomDeLaFormule, prixDAbonnement);
        return abonnement;
    }

    private Abonnement(String abonnementId, String clientId, PrixDAbonnement prixDAbonnement, String nomDeLaFormule) {
        this.abonnementId = abonnementId;
        this.prixDAbonnement = prixDAbonnement;
        this.clientId = clientId;
        this.nomDeLaFormule = nomDeLaFormule;
    }

    public DomainEvent getEvent() {
        return event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Abonnement that = (Abonnement) o;
        return Objects.equals(abonnementId, that.abonnementId) &&
                Objects.equals(prixDAbonnement, that.prixDAbonnement) &&
                Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(abonnementId, prixDAbonnement, clientId);
    }
}
