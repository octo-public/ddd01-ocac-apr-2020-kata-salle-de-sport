package salle_de_sport.abonnement;

import java.util.HashMap;
import java.util.Map;

public class InMemoryAbonnementRepository implements AbonnementRepository {
    private Map<String, Abonnement> abonnements = new HashMap<>();

    @Override
    public Abonnement getById(String abonnementId) {
        return abonnements.get(abonnementId);
    }

    @Override
    public void save(Abonnement abonnement) {
        abonnements.put(abonnement.abonnementId, abonnement);
    }
}
