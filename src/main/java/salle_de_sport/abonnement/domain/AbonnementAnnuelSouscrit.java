package salle_de_sport.abonnement.domain;

import salle_de_sport.abonnement.DomainEvent;
import salle_de_sport.abonnement.PrixDAbonnement;

import java.util.Objects;

public class AbonnementAnnuelSouscrit implements DomainEvent {
    private final String abonnementId;
    private final String nomDeLaFormule;
    private final PrixDAbonnement prixDeLAbonnement;

    public AbonnementAnnuelSouscrit(String abonnementId, String nomDeLaFormule, PrixDAbonnement prixDeLAbonnement) {
        this.abonnementId = abonnementId;
        this.nomDeLaFormule = nomDeLaFormule;
        this.prixDeLAbonnement = prixDeLAbonnement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbonnementAnnuelSouscrit that = (AbonnementAnnuelSouscrit) o;
        return Objects.equals(abonnementId, that.abonnementId) &&
                Objects.equals(nomDeLaFormule, that.nomDeLaFormule) &&
                Objects.equals(prixDeLAbonnement, that.prixDeLAbonnement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(abonnementId, nomDeLaFormule, prixDeLAbonnement);
    }
}
