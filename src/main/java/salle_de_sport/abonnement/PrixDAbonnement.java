package salle_de_sport.abonnement;

import java.util.Objects;

public class PrixDAbonnement {
    public final double prix;

    public PrixDAbonnement(double prix) {
        this.prix = prix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrixDAbonnement that = (PrixDAbonnement) o;
        return Double.compare(that.prix, prix) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prix);
    }
}
