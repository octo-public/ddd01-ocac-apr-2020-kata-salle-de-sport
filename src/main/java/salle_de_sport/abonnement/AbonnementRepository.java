package salle_de_sport.abonnement;

public interface AbonnementRepository {
    Abonnement getById(String abonnementId);

    void save(Abonnement abonnement);
}
