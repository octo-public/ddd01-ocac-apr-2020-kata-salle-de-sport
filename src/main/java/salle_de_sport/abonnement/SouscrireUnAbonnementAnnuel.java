package salle_de_sport.abonnement;

public class SouscrireUnAbonnementAnnuel {
    public final String nomDeLaFormule;
    public final PrixDeBase prixDeBase;
    public final String clientId;

    public SouscrireUnAbonnementAnnuel(String nomDeLaFormule, PrixDeBase prixDeBase, String clientId) {
        this.nomDeLaFormule = nomDeLaFormule;
        this.prixDeBase = prixDeBase;
        this.clientId = clientId;
    }
}
