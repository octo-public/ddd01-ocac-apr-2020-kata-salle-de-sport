package salle_de_sport.abonnement;

public class PrixDeBase {
    public final float prix;

    public PrixDeBase(float prix) {
        this.prix = prix;
    }

    public PrixDAbonnement appliquer(Reduction reduction) {
        return new PrixDAbonnement(prix * (1 - reduction.reduction));
    }
}
