package salle_de_sport.abonnement;

public class SouscrireUnAbonnementAnnuelUseCase {
    private final AbonnementRepository abonnementRepository;

    public SouscrireUnAbonnementAnnuelUseCase(AbonnementRepository abonnementRepository) {
        this.abonnementRepository = abonnementRepository;
    }

    public DomainEvent execute(SouscrireUnAbonnementAnnuel souscrireUnAbonnementAnnuel) {
        String abonnementId = "abonnementId";

        Abonnement abonnement = Abonnement
                .souscrireAbonnementAnnuel(abonnementId, souscrireUnAbonnementAnnuel.clientId, souscrireUnAbonnementAnnuel.prixDeBase, souscrireUnAbonnementAnnuel.nomDeLaFormule);

        abonnementRepository.save(abonnement);

        return abonnement.getEvent();
    }
}
